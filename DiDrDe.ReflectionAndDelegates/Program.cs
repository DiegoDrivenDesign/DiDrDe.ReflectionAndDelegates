﻿using System;
using System.Linq;
using System.Threading.Tasks;

namespace DiDrDe.ReflectionAndDelegates
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var myDelegate = new Func<Task>(() => Console.Out.WriteLineAsync("This is inside my delegate execution!"));
            var handler = new Handler();
            var handlerType = typeof(Handler);
            var handleMethod =
                handlerType
                    .GetMethods()
                    .First(x => x.ReturnType == typeof(Task));

            var myObject =
                new
                {
                    Foo = "bar"
                };

            var parameters =
                new[]
                {
                    (object)myObject,
                    myDelegate
                };
            try
            {
                await (Task)handleMethod.Invoke(handler, parameters);
            }
            catch (Exception exception)
            {
                await Console.Out.WriteLineAsync(exception.Message);
            }

            Console.ReadLine();
        }
    }
}
